# HTTP-Weiterleitungen für grammle.de

In diesem Repo liegen nur ein paar HTML-Dateien mit HTTP-Weiterleitungen für
bestimmte Subdomains von grammle.de. Weil die Inhalte dieses Repos nicht die
notwendige künstlerische Schöpfungshöhe erreichen, sind sie nicht urheberrechtlich
geschützt.